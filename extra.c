#include <emmintrin.h>
#include <nmmintrin.h>
#include <omp.h>
#include <stdio.h>
#include <string.h>

float padded[(3000)*(3000)];
float newkern[(3000)*(3000)];

// #define uint unsigned int

int conv2D(float* in, float* out, const uint data_size_X, const uint data_size_Y,
           float* kernel, const int kernel_x, const int kernel_y)
{
  //setting 8 threads is actually better than 16
  omp_set_num_threads(8);
  const int kern_cent_X = (kernel_x - 1)/2;
  const int kern_cent_Y = (kernel_y - 1)/2;

  const uint padX = data_size_X + kernel_x - 1;


  // padding

#pragma omp parallel for
  for(uint y=0; y<data_size_Y; y++) {
    memset(&padded[(y+kern_cent_Y)*padX], 0, kern_cent_X*sizeof(float));
    memset(&padded[(data_size_X+kern_cent_X) + (y+kern_cent_Y)*padX],
           0, kern_cent_X*sizeof(float));

    memcpy(&padded[kern_cent_X+(y+kern_cent_Y)*padX],
           &in[y*data_size_X], data_size_X*sizeof(float));

  }


// #pragma omp parallel for
    for(int y=0; y<kern_cent_Y; y++) {
      memset(&padded[y*padX], 0, padX*sizeof(float));
      memset(&padded[(data_size_Y+kern_cent_Y+y)*padX], 0,
             padX*sizeof(float));
    }


#pragma omp parallel for
    for (int y = 0; y < kernel_y; y++) {
      for(int x = 0; x < kernel_x; x++) {
        newkern[x+y*kernel_x] = kernel[x+y*kernel_x];
      }
    }

    int out_y_temp;
    __m128 c1, c2, c3, c4, c5, c6, c7, a1, b1, b2, b3, b4, b5, b6, b7;

    // main convolution loop

#pragma omp parallel for private(out_y_temp, c1, c2, c3, c4, c5, c6, c7, a1, b1, b2, b3, b4, b5, b6, b7)
    for(int y = kern_cent_Y; y < data_size_Y + kern_cent_Y; y++) {
      out_y_temp = (y-kern_cent_Y) * data_size_X;
      int x;
      for(x = kern_cent_X; x < ((data_size_X)/28*28) + kern_cent_X; x+=28) {
        c1 = _mm_setzero_ps();
        c2 = _mm_setzero_ps();
        c3 = _mm_setzero_ps();
        c4 = _mm_setzero_ps();
        c5 = _mm_setzero_ps();
        c6 = _mm_setzero_ps();
        c7 = _mm_setzero_ps();

        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++) {
          for(int i = -kern_cent_X; i <= kern_cent_X; i++) {
            //Note that the kernel is flipped
            a1 = _mm_load1_ps(newkern + (kern_cent_X-i)+(kern_cent_Y-(j))*kernel_x);

            b1 = _mm_loadu_ps(padded + (x+i+0) + (y+j)*padX);
            b2 = _mm_loadu_ps(padded + (x+i+4) + (y+j)*padX);
            b3 = _mm_loadu_ps(padded + (x+i+8) + (y+j)*padX);
            b4 = _mm_loadu_ps(padded + (x+i+12) + (y+j)*padX);
            b5 = _mm_loadu_ps(padded + (x+i+16) + (y+j)*padX);
            b6 = _mm_loadu_ps(padded + (x+i+20) + (y+j)*padX);
            b7 = _mm_loadu_ps(padded + (x+i+24) + (y+j)*padX);


            b1 = _mm_mul_ps(a1, b1);
            b2 = _mm_mul_ps(a1, b2);
            b3 = _mm_mul_ps(a1, b3);
            b4 = _mm_mul_ps(a1, b4);
            b5 = _mm_mul_ps(a1, b5);
            b6 = _mm_mul_ps(a1, b6);
            b7 = _mm_mul_ps(a1, b7);

            c1 = _mm_add_ps(c1, b1);
            c2 = _mm_add_ps(c2, b2);
            c3 = _mm_add_ps(c3, b3);
            c4 = _mm_add_ps(c4, b4);
            c5 = _mm_add_ps(c5, b5);
            c6 = _mm_add_ps(c6, b6);
            c7 = _mm_add_ps(c7, b7);
          }
        }
        _mm_storeu_ps(out+(x+0)-kern_cent_X + out_y_temp, c1);
        _mm_storeu_ps(out+(x+4)-kern_cent_X + out_y_temp, c2);
        _mm_storeu_ps(out+(x+8)-kern_cent_X + out_y_temp, c3);
        _mm_storeu_ps(out+(x+12)-kern_cent_X + out_y_temp, c4);
        _mm_storeu_ps(out+(x+16)-kern_cent_X + out_y_temp, c5);
        _mm_storeu_ps(out+(x+20)-kern_cent_X + out_y_temp, c6);
        _mm_storeu_ps(out+(x+24)-kern_cent_X + out_y_temp, c7);
      }

      int leftover = data_size_X - (data_size_X)/28*28;
      x = (data_size_X)/28*28 + kern_cent_X;

      if (leftover >= 28) {
        c1 = _mm_setzero_ps();
        c2 = _mm_setzero_ps();
        c3 = _mm_setzero_ps();
        c4 = _mm_setzero_ps();
        c5 = _mm_setzero_ps();
        c6 = _mm_setzero_ps();
        c7 = _mm_setzero_ps();

        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++) {
          for(int i = -kern_cent_X; i <= kern_cent_X; i++) {
            a1 = _mm_load1_ps(newkern + (kern_cent_X-i)+(kern_cent_Y-(j))*kernel_x);
            b1 = _mm_loadu_ps(padded + (x+i+0) + (y+j)*padX);
            b2 = _mm_loadu_ps(padded + (x+i+4) + (y+j)*padX);
            b3 = _mm_loadu_ps(padded + (x+i+8) + (y+j)*padX);
            b4 = _mm_loadu_ps(padded + (x+i+12) + (y+j)*padX);
            b5 = _mm_loadu_ps(padded + (x+i+16) + (y+j)*padX);
            b6 = _mm_loadu_ps(padded + (x+i+20) + (y+j)*padX);
            b7 = _mm_loadu_ps(padded + (x+i+24) + (y+j)*padX);

            b1 = _mm_mul_ps(a1, b1);
            b2 = _mm_mul_ps(a1, b2);
            b3 = _mm_mul_ps(a1, b3);
            b4 = _mm_mul_ps(a1, b4);
            b5 = _mm_mul_ps(a1, b5);
            b6 = _mm_mul_ps(a1, b6);
            b7 = _mm_mul_ps(a1, b7);

            c1 = _mm_add_ps(c1, b1);
            c2 = _mm_add_ps(c2, b2);
            c3 = _mm_add_ps(c3, b3);
            c4 = _mm_add_ps(c4, b4);
            c5 = _mm_add_ps(c5, b5);
            c6 = _mm_add_ps(c6, b6);
            c7 = _mm_add_ps(c7, b7);
          }
        }
        _mm_storeu_ps(out+(x+0)-kern_cent_X + out_y_temp, c1);
        _mm_storeu_ps(out+(x+4)-kern_cent_X + out_y_temp, c2);
        _mm_storeu_ps(out+(x+8)-kern_cent_X + out_y_temp, c3);
        _mm_storeu_ps(out+(x+12)-kern_cent_X + out_y_temp, c4);
        _mm_storeu_ps(out+(x+16)-kern_cent_X + out_y_temp, c5);
        _mm_storeu_ps(out+(x+20)-kern_cent_X + out_y_temp, c6);
        _mm_storeu_ps(out+(x+24)-kern_cent_X + out_y_temp, c7);
        x+=28;
      }

      else if (leftover >= 24) {
        c1 = _mm_setzero_ps();
        c2 = _mm_setzero_ps();
        c3 = _mm_setzero_ps();
        c4 = _mm_setzero_ps();
        c5 = _mm_setzero_ps();
        c6 = _mm_setzero_ps();

        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++) {
          for(int i = -kern_cent_X; i <= kern_cent_X; i++) {
            a1 = _mm_load1_ps(newkern + (kern_cent_X-i)+(kern_cent_Y-(j))*kernel_x);
            b1 = _mm_loadu_ps(padded + (x+i+0) + (y+j)*padX);
            b2 = _mm_loadu_ps(padded + (x+i+4) + (y+j)*padX);
            b3 = _mm_loadu_ps(padded + (x+i+8) + (y+j)*padX);
            b4 = _mm_loadu_ps(padded + (x+i+12) + (y+j)*padX);
            b5 = _mm_loadu_ps(padded + (x+i+16) + (y+j)*padX);
            b6 = _mm_loadu_ps(padded + (x+i+20) + (y+j)*padX);

            b1 = _mm_mul_ps(a1, b1);
            b2 = _mm_mul_ps(a1, b2);
            b3 = _mm_mul_ps(a1, b3);
            b4 = _mm_mul_ps(a1, b4);
            b5 = _mm_mul_ps(a1, b5);
            b6 = _mm_mul_ps(a1, b6);

            c1 = _mm_add_ps(c1, b1);
            c2 = _mm_add_ps(c2, b2);
            c3 = _mm_add_ps(c3, b3);
            c4 = _mm_add_ps(c4, b4);
            c5 = _mm_add_ps(c5, b5);
            c6 = _mm_add_ps(c6, b6);

          }
        }
        _mm_storeu_ps(out+(x+0)-kern_cent_X + out_y_temp, c1);
        _mm_storeu_ps(out+(x+4)-kern_cent_X + out_y_temp, c2);
        _mm_storeu_ps(out+(x+8)-kern_cent_X + out_y_temp, c3);
        _mm_storeu_ps(out+(x+12)-kern_cent_X + out_y_temp, c4);
        _mm_storeu_ps(out+(x+16)-kern_cent_X + out_y_temp, c5);
        _mm_storeu_ps(out+(x+20)-kern_cent_X + out_y_temp, c6);
        x+=24;
      }

      else if (leftover >= 20) {

        c1 = _mm_setzero_ps();
        c2 = _mm_setzero_ps();
        c3 = _mm_setzero_ps();
        c4 = _mm_setzero_ps();
        c5 = _mm_setzero_ps();

        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++) {
          for(int i = -kern_cent_X; i <= kern_cent_X; i++) {

            a1 = _mm_load1_ps(newkern + (kern_cent_X-i)+(kern_cent_Y-(j))*kernel_x);
            b1 = _mm_loadu_ps(padded + (x+i+0) + (y+j)*padX);
            b2 = _mm_loadu_ps(padded + (x+i+4) + (y+j)*padX);
            b3 = _mm_loadu_ps(padded + (x+i+8) + (y+j)*padX);
            b4 = _mm_loadu_ps(padded + (x+i+12) + (y+j)*padX);
            b5 = _mm_loadu_ps(padded + (x+i+16) + (y+j)*padX);

            b1 = _mm_mul_ps(a1, b1);
            b2 = _mm_mul_ps(a1, b2);
            b3 = _mm_mul_ps(a1, b3);
            b4 = _mm_mul_ps(a1, b4);
            b5 = _mm_mul_ps(a1, b5);

            c1 = _mm_add_ps(c1, b1);
            c2 = _mm_add_ps(c2, b2);
            c3 = _mm_add_ps(c3, b3);
            c4 = _mm_add_ps(c4, b4);
            c5 = _mm_add_ps(c5, b5);

          }
        }
        _mm_storeu_ps(out+(x+0)-kern_cent_X + out_y_temp, c1);
        _mm_storeu_ps(out+(x+4)-kern_cent_X + out_y_temp, c2);
        _mm_storeu_ps(out+(x+8)-kern_cent_X + out_y_temp, c3);
        _mm_storeu_ps(out+(x+12)-kern_cent_X + out_y_temp, c4);
        _mm_storeu_ps(out+(x+16)-kern_cent_X + out_y_temp, c5);
        x+=20;
      }

      else if (leftover >= 16) {

        c1 = _mm_setzero_ps();
        c2 = _mm_setzero_ps();
        c3 = _mm_setzero_ps();
        c4 = _mm_setzero_ps();

        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++) {
          for(int i = -kern_cent_X; i <= kern_cent_X; i++) {

            a1 = _mm_load1_ps(newkern + (kern_cent_X-i)+(kern_cent_Y-(j))*kernel_x);
            b1 = _mm_loadu_ps(padded + (x+i+0) + (y+j)*padX);
            b2 = _mm_loadu_ps(padded + (x+i+4) + (y+j)*padX);
            b3 = _mm_loadu_ps(padded + (x+i+8) + (y+j)*padX);
            b4 = _mm_loadu_ps(padded + (x+i+12) + (y+j)*padX);

            b1 = _mm_mul_ps(a1, b1);
            b2 = _mm_mul_ps(a1, b2);
            b3 = _mm_mul_ps(a1, b3);
            b4 = _mm_mul_ps(a1, b4);

            c1 = _mm_add_ps(c1, b1);
            c2 = _mm_add_ps(c2, b2);
            c3 = _mm_add_ps(c3, b3);
            c4 = _mm_add_ps(c4, b4);
          }
        }
        _mm_storeu_ps(out+(x+0)-kern_cent_X + out_y_temp, c1);
        _mm_storeu_ps(out+(x+4)-kern_cent_X + out_y_temp, c2);
        _mm_storeu_ps(out+(x+8)-kern_cent_X + out_y_temp, c3);
        _mm_storeu_ps(out+(x+12)-kern_cent_X + out_y_temp, c4);
        x+=16;
      }

      else if (leftover >= 12) {
        c1 = _mm_setzero_ps();
        c2 = _mm_setzero_ps();
        c3 = _mm_setzero_ps();

        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++) {
          for(int i = -kern_cent_X; i <= kern_cent_X; i++) {
            a1 = _mm_load1_ps(newkern + (kern_cent_X-i)+(kern_cent_Y-(j))*kernel_x);
            b1 = _mm_loadu_ps(padded + (x+i+0) + (y+j)*padX);
            b2 = _mm_loadu_ps(padded + (x+i+4) + (y+j)*padX);
            b3 = _mm_loadu_ps(padded + (x+i+8) + (y+j)*padX);

            b1 = _mm_mul_ps(a1, b1);
            b2 = _mm_mul_ps(a1, b2);
            b3 = _mm_mul_ps(a1, b3);

            c1 = _mm_add_ps(c1, b1);
            c2 = _mm_add_ps(c2, b2);
            c3 = _mm_add_ps(c3, b3);
          }
        }
        _mm_storeu_ps(out+(x+0)-kern_cent_X + out_y_temp, c1);
        _mm_storeu_ps(out+(x+4)-kern_cent_X + out_y_temp, c2);
        _mm_storeu_ps(out+(x+8)-kern_cent_X + out_y_temp, c3);
        x+=12;
      }

      else if (leftover >= 8) {
        c1 = _mm_setzero_ps();
        c2 = _mm_setzero_ps();

        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++) {
          for(int i = -kern_cent_X; i <= kern_cent_X; i++) {
            a1 = _mm_load1_ps(newkern + (kern_cent_X-i)+(kern_cent_Y-(j))*kernel_x);
            b1 = _mm_loadu_ps(padded + (x+i+0) + (y+j)*padX);
            b2 = _mm_loadu_ps(padded + (x+i+4) + (y+j)*padX);

            b1 = _mm_mul_ps(a1, b1);
            b2 = _mm_mul_ps(a1, b2);

            c1 = _mm_add_ps(c1, b1);
            c2 = _mm_add_ps(c2, b2);
          }
        }
        _mm_storeu_ps(out+(x+0)-kern_cent_X + out_y_temp, c1);
        _mm_storeu_ps(out+(x+4)-kern_cent_X + out_y_temp, c2);
        x+=8;
      }

      else if (leftover >= 4) {

        c1 = _mm_setzero_ps();

        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++) {
          for(int i = -kern_cent_X; i <= kern_cent_X; i++) {
            a1 = _mm_load1_ps(newkern + (kern_cent_X-i)+(kern_cent_Y-(j))*kernel_x);
            b1 = _mm_loadu_ps(padded + (x+i+0) + (y+j)*padX);
            c1 = _mm_add_ps(c1, _mm_mul_ps(a1, b1));
          }
        }
        _mm_storeu_ps(out+(x+0)-kern_cent_X + out_y_temp, c1);
        x+=4;
      }

      for (; x < (data_size_X + kern_cent_X); x++) {
        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++) {
          for(int i = -kern_cent_X; i <= kern_cent_X; i++) {
            out[(x)-kern_cent_X + out_y_temp] += newkern[(kern_cent_X-i)+(kern_cent_Y-(j))*kernel_x] * padded[(x+i) + (y+j)*padX];
          }
        }
      }
    }
}
